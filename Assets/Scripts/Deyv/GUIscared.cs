﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GUIscared : MonoBehaviour {

    [Range(0f, 1f)]
    public float ScaredLevel;
    public Image motherDamagedBar;
    public Image fatherDamagedBar;
    public Image kidDamagedBar;

    GameManagerClass GM;
    private void Start()
    {
         GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManagerClass>();
    }
    private void Update()
    {
       fatherDamagedBar.fillAmount = GM.fatherScared;
       motherDamagedBar.fillAmount = GM.motherScared;
       kidDamagedBar.fillAmount = GM.kidScared;
    }
   
}
