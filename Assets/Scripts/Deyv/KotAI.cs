﻿using System.Collections.Generic;
using UnityEngine;

public class KotAI : MonoBehaviour {
    [SerializeField] float minDistance;
    [SerializeField] float speed;
    [SerializeField] float highPosition;
    [SerializeField] float downPosition;

    public AudioSource audioComp1;
    public AudioSource audioComp2;


    public bool canMove;
    Vector3 _newPosition;
    bool changedSprite = false;
    public GameObject scaredIMG;
    public GameObject calmIMG;
    GameObject Player;
    bool isplaying;
    bool isplaying2;
    GameObject target;
    public GameObject pointA;
    public GameObject pointB;

    bool changeDirection;
    private void Start()
    {

        Player = GameObject.FindGameObjectWithTag("Player");
        if (Player==null)
            Debug.Log("player null");
        target = pointA;
    }
    private void Update()
    {
        float DistancePlayerKot = Vector3.Distance(this.transform.position, Player.transform.position);
        
        if(DistancePlayerKot < minDistance)
        {
            if (!canMove)
            {
                if (!isplaying)
                {
                    audioComp1.Play();
                    isplaying = true;
                    if(!audioComp1.isPlaying)
                    audioComp1.enabled = false;
                }
                
                if (transform.position.y > highPosition - 0.3f)
                 {
                     scaredIMG.SetActive(true);
                     calmIMG.SetActive(false);
                     changedSprite = true;
                     changeDirection = true;
                 }
                if (changedSprite && changeDirection)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, downPosition, transform.position.z), speed * Time.deltaTime);
                }
                if(transform.position.y < downPosition + 0.2f)
                {
                    changeDirection = false;
                }
                if (!changeDirection && changedSprite)
                {
                    canMove = true;
                }
                if(!changedSprite && !changeDirection)
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, highPosition + 1f, transform.position.z), speed * Time.deltaTime);
                }
            }
        }
        if (canMove)
        {
            if (!isplaying2)
            {
                audioComp2.Play();
                isplaying2 = true;
            }
            _newPosition = transform.position;
                _newPosition.y += Mathf.Sin(Time.time*20) * Time.deltaTime;
                transform.position = _newPosition;
            
            if(transform.position.x < pointA.transform.position.x + 0.3f)
            {
                target = pointB;
            }
            if (transform.position.x > pointB.transform.position.x - 0.3f)
            {
                target = pointA;
            }
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x, transform.position.y, transform.position.z), speed * Time.deltaTime);
        }
    }
}
