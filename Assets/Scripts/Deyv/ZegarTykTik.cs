﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZegarTykTik : InteractiveObject {
    float pointA = -6;
    float pointB = 4;
    float target;
    bool stop;
    AudioSource tykanie;
    private void Start()
    {
        tykanie = GetComponent<AudioSource>();
        target = pointA;
    }
    private void Update()
    {
        if (transform.rotation.eulerAngles.z <= 359 && transform.rotation.eulerAngles.z >=356f)
        {

            target = pointB;
        }

        if (transform.rotation.eulerAngles.z >= 3.8 && transform.rotation.eulerAngles.z <=5f)
        {
            target = pointA;
        }
        if (!stop)
        {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0,0,target)), .15f);
            if (!tykanie.isPlaying)
            {
                tykanie.Play();
            }
        }
        if (stop)
        {
            StartCoroutine(restart());
            tykanie.Stop();
        }
    }
    public override void OnGhostInteraction()
    {
        base.OnGhostInteraction();
        stop = true;
    }
    IEnumerator restart()
    {
            yield return new WaitForSeconds(4f);
        stop = false;
    }
}
