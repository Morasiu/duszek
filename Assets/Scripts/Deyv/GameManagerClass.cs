﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManagerClass : MonoBehaviour {

    public float motherScared =0 ;
    public float fatherScared = 0;
    public float kidScared = 0 ;
      GameObject player;
    public GameObject img;
    PlayerController PlayerScript;
    bool wczytano;
    public Cinemachine.CinemachineVirtualCamera cam;


    void Start () {
        DontDestroyOnLoad(this.gameObject);
        player = GameObject.FindGameObjectWithTag("Player");
        if(player == null)
        {
            return;
        }
         PlayerScript = player.GetComponent<PlayerController>();
        if(PlayerScript == null)
        {
            return;
        }
    }
    private void Update()
    {
        if (player == null)
        {
            return;
        }
        if (PlayerScript == null)
        {
            return;
        }
        if (PlayerScript.gameOver == true)
        {
            Debug.Log("cam is null");
            img.gameObject.SetActive(true);
        }
        if (!player.activeSelf )
        {
            if (!wczytano)
            {
                if (player != null)
                    StartCoroutine(Reload());
                else return;
            }
            else return;
        }
    }
    IEnumerator Reload()
    {
       yield return new WaitForSeconds(3f);
        if (player != null)
        {
            if (!player.activeSelf)
            {
                img.gameObject.SetActive(false);
                SceneManager.LoadScene("CreditsScene", LoadSceneMode.Single);
                player.SetActive(true);
                wczytano = true;

            }
        }
            
    }


}
