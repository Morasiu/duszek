﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundClicker : MonoBehaviour {

    AudioSource clipClick;
    
	void Start () {
        clipClick = GetComponent<AudioSource>();
	}
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Mouse0)||Input.GetKeyDown(KeyCode.Space))
        {
            clipClick.Play();
        }
	}
}
