﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MusicSliderMenu : MonoBehaviour {

    AudioSource music;
    public Slider slider;
    private void Start()
    {
        music = GetComponent<AudioSource>();
        slider = GetComponent<Slider>();
    }

    void changeMusic()
    {
        music.volume = slider.value;
    }

}
