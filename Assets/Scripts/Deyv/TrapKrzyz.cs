﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;
using System.Text;

class TrapKrzyz : InteractiveObject
{
    bool madeDamage;
    GameManagerClass GM;
	public HouseHolder houseHolder;
    private void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManagerClass>();
        if (GM == null)
        {
            Debug.Log("dziwny bug");
        }

    }
    public override void OnGhostInteraction()
    {
        StartCoroutine(Obroc());
        if (!madeDamage) {
			GetComponent<AudioSource>().Play();
			StartCoroutine(Scare());
			GM.motherScared += 0.2f;
			madeDamage = true;
		}

	}

	private IEnumerator Scare() {
		yield return new WaitForSeconds(1);
		houseHolder.IncreaseFearLevel();
	}

	public IEnumerator Obroc()
    {
        
        while (transform.rotation.eulerAngles.z <= 180)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, 179)), .2f);

        yield return new WaitForSeconds(.03f);
        }
    }
}