﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameoverMovement : MonoBehaviour {
    public float pointA;
    public float pointB;
    float target;
    //bujanie
    public Vector3 pointX;
    //gorna pozycja


    private void Start()
    {
        target = pointA;
    }
    private void Update()
    {
       // if(transform.position.y <pointX.y-0.2f)
       // transform.position = Vector3.MoveTowards(transform.position, pointX, 0.2f);
        //jedzie do gory
        if (transform.rotation.eulerAngles.z <= pointA + 0.6f)
            target = pointB;
        if (transform.rotation.eulerAngles.z >= pointB - 0.6f)
            target = pointA;

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, target)), .1f);

    }
}
