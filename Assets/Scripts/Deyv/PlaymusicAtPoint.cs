﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaymusicAtPoint : MonoBehaviour {

    public AudioClip clip; //make sure you assign an actual clip here in the inspector

    void Start()
    {
        AudioSource.PlayClipAtPoint(clip, new Vector3(8, 0, 0));
    }
}
