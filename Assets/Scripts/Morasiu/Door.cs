﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour {

	public RoomChanging roomChanging;
	public SceneField SceneToLoad;
	public Transform DoorToTeleport;

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.transform.tag == "Player") {
			if (roomChanging == RoomChanging.LoadNewScene)
				SceneManager.LoadScene(SceneToLoad);
			else if (roomChanging == RoomChanging.TeleportToRoom) {
				var goTransform = collision.gameObject.transform;
				goTransform.position = new Vector3(DoorToTeleport.position.x, goTransform.position.y, goTransform.position.z);
			}
		}
	}

	public enum RoomChanging {
		LoadNewScene,
		TeleportToRoom
	}
}
