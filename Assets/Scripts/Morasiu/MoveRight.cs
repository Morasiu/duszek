﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRight : MonoBehaviour {
	public float speed = 3f;
	public float Sin = 20f;
	Vector3 _newPosition;

	void Update () {
		_newPosition = transform.position;
		_newPosition.y += Mathf.Sin(Time.time * Sin) * Time.deltaTime;
		transform.position = _newPosition;
	}

	private void LateUpdate() {
		transform.Translate(Vector3.right * speed * Time.deltaTime);
	}
}
