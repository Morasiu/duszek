﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vaze : InteractiveObject {
	public Collider2D Collider2D;
	public HouseHolder houseHolder;
	public GameObject Broken;

	public override void OnGhostInteraction() {
		GetComponent<Rigidbody2D>().gravityScale = 1;
		Collider2D.enabled = true;
	}

	private void OnCollisionEnter2D(Collision2D collision) {
		if (collision.transform.tag != "Player")
			if (Broken != null) {
				houseHolder.IncreaseFearLevel();
				GetComponent<AudioSource>().Play();
				Broken.SetActive(true);
				GetComponent<SpriteRenderer>().enabled = false;
				Collider2D.enabled = false;
			}
	}
}