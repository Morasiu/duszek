﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReleaseCamera : MonoBehaviour {

	public float TimeToLoadNewScene = 1f;

	public void Release() {
		var cinemachine = GetComponent<Cinemachine.CinemachineVirtualCamera>();
		cinemachine.Follow = null;
		StartCoroutine(LoadCutscene());
	}

	private IEnumerator LoadCutscene() {
		yield return new WaitForSeconds(TimeToLoadNewScene);
		SceneManager.LoadScene("Cutscene");
	}
}
