﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	public SceneField SceneToLoad;
	public float TimeToLoad;

	void Start () {
		StartCoroutine(LoadScene());
	}

	private IEnumerator LoadScene() {
		yield return new WaitForSeconds(TimeToLoad);
		SceneManager.LoadScene(SceneToLoad.SceneName);
	}
}
