﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {
	private const float RotationSpeed = 0.2f;
	public bool FacingRight = true;
	public Transform FlippingObject;
	private bool IsRotating;

	void Start () {
		StartCoroutine(CheckForRotation());
	}

	IEnumerator CheckForRotation() {
		var start = FlippingObject.rotation;
		var vectorStart = new Vector3(0, 1, 0);
		var vectorFlipped = new Vector3(0, 178, 0);
		while (true) {
			var firstPositionX = FlippingObject.position.x;
			yield return new WaitForEndOfFrame();
            // TODO Calclulate targetPosition based on FacingRight
            // because this solution make not exactly full turn and it's slowly collapsing


			if (FlippingObject.position.x > firstPositionX && FacingRight) {     
				var targetRotation = Quaternion.Euler(vectorStart);
				while (Mathf.Round(FlippingObject.rotation.eulerAngles.y) > vectorStart.y + 3) {
					FlippingObject.rotation = Quaternion.Slerp(FlippingObject.rotation,
						targetRotation, RotationSpeed);
					yield return new WaitForEndOfFrame();
				}
				FacingRight = false;
			} else if (FlippingObject.position.x < firstPositionX && !FacingRight) {
				var targetRotation = Quaternion.Euler(vectorFlipped);
				while (Mathf.Round(FlippingObject.rotation.eulerAngles.y) < vectorFlipped.y - 1) {
					FlippingObject.rotation = Quaternion.Slerp(FlippingObject.rotation,
						targetRotation, RotationSpeed);
					yield return new WaitForEndOfFrame();
				}
				FacingRight = true;
			}
		}
	}
}
