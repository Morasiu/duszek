﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBoo : MonoBehaviour {

	public float BooingTime = 1f;
	[HideInInspector]
	public bool IsBooing = false;

	public AudioSource audioSource;
	public List<AudioClip> boos;
	public Sprite BooSprite;

	private Sprite DefaultSprite;
	public SpriteRenderer SpriteRenderer;
	private void Start() {
		DefaultSprite = SpriteRenderer.sprite;
	}

	void Update () {
		if(Input.GetKeyUp(KeyCode.Space) && !IsBooing) {
			StartCoroutine(ShowBoo());
		}
	}

	IEnumerator ShowBoo() {
		IsBooing = true;
		var audioClip = boos[Random.Range(0, boos.Count)];
		SpriteRenderer.sprite = BooSprite;
		audioSource.clip = audioClip;
		audioSource.Play();
		yield return new WaitForSeconds(BooingTime);
		SpriteRenderer.sprite = DefaultSprite;
		IsBooing = false;
	}
}
