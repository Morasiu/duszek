﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseHolder : MonoBehaviour {

	public FearLevel FearLevelValue = FearLevel.One;
	public float MoveSprite = 0.1f;

	public List<AudioClip> audioClips;
	public AudioClip level3audioClip;
	public Sprite LevelTwo;
	public Sprite LevelThree;
	private float RotationSpeed = 0.2f;

	public virtual void IncreaseFearLevel() {
		if (FearLevelValue == FearLevel.One) {
			FearLevelValue = FearLevel.Two;
		} else if (FearLevelValue == FearLevel.Two) {
			FearLevelValue = FearLevel.Three;
		} else if (FearLevelValue == FearLevel.Three) {
			FearLevelValue = FearLevel.Four;
		}

		if(FearLevelValue == FearLevel.Four) {
			StartCoroutine(GoUp());
			return;
		}

		StartCoroutine(MoveSpriteUp());
		var audio = GetComponent<AudioSource>();
		if (FearLevelValue == FearLevel.Three) {
			audio.clip = level3audioClip;
			audio.Play();
		}
		else if(audioClips.Count > 0) {
			audio.clip = audioClips[UnityEngine.Random.Range(0, audioClips.Count)];
			audio.Play();
		}
	}


	private IEnumerator MoveSpriteUp() {
		var start = transform.rotation;
		var vectorStart = new Vector3(0, 1, 0);
		var vectorFlipped = new Vector3(0, 178, 0);
		bool canFlip = true;
		if (FearLevelValue == FearLevel.Two) {
			var targetRotation = Quaternion.Euler(vectorFlipped);
			while (Mathf.Round(transform.rotation.eulerAngles.y) < vectorFlipped.y - 3) {
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, RotationSpeed);
				if (Mathf.Round(transform.rotation.eulerAngles.y) - 90 > 0 && canFlip) {
					canFlip = false;
					GetComponent<SpriteRenderer>().sprite = LevelTwo;
					//transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
				}
				yield return new WaitForEndOfFrame();
			}
			canFlip = true;

		} else if (FearLevelValue == FearLevel.Three) {
			var targetRotation = Quaternion.Euler(vectorStart);
			while (Mathf.Round(transform.rotation.eulerAngles.y) > vectorStart.y - 1) {
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, RotationSpeed);
				if (Mathf.Round(transform.rotation.eulerAngles.y) - 90 > 0 && canFlip) {
					canFlip = false;
					GetComponent<SpriteRenderer>().sprite = LevelThree;
				//	transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
				}
				yield return new WaitForEndOfFrame();
			}
			canFlip = true;
		}
		//transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y + 180f, 0);
	}

	public IEnumerator GoUp() {
		while(true) {
			transform.parent.transform.Translate(Vector3.up * MoveSprite * Time.deltaTime);
			yield return new WaitForEndOfFrame();
		}
	}
}

public enum FearLevel {
	One, 
	Two,
	Three,
	Four
}

