﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CollisionSounds : MonoBehaviour {
	AudioSource audioSource;

	[SerializeField]
	private List<AudioClip> audioClips;

	private void Start() {
		audioSource = GetComponent<AudioSource>();
	}

	private void OnCollisionEnter2D(Collision2D collision) {
		if(!audioSource.isPlaying) {
			audioSource.clip = audioClips[Random.Range(0, audioClips.Count)];
			audioSource.Play();
		}
	}
}
