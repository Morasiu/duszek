﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObject : MonoBehaviour {

	private void OnTriggerStay2D(Collider2D collision) {
		if (collision.transform.tag == "Player") {
			var boo = collision.GetComponent<GhostBoo>();
			if (!boo.IsBooing) return;
			boo.IsBooing = false;
			OnGhostInteraction();
		}
	}

	public virtual void OnGhostInteraction() { }
}