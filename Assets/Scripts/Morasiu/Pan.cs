﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pan : InteractiveObject {
	public HouseHolder houseHolder;
	public Sprite Broken;

	public override void OnGhostInteraction() {
		GetComponent<Rigidbody2D>().gravityScale = 1;
		transform.GetChild(0).gameObject.SetActive(true);
		if (Broken != null) GetComponent<SpriteRenderer>().sprite = Broken;
		StartCoroutine(Scare());
	}


	private IEnumerator Scare() {
		yield return new WaitForSeconds(1);
		houseHolder.IncreaseFearLevel();
	}
}
