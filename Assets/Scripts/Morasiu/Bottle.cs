﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Morasiu {
	class Bottle : InteractiveObject {
		public Collider2D Collider2D;
		public HouseHolder houseHolder;
		public Sprite Broken;

		bool Scared = false;

		public override void OnGhostInteraction() {
			GetComponent<Rigidbody2D>().gravityScale = 1;
			Collider2D.enabled = true;
		}

		private void OnCollisionEnter2D(Collision2D collision) {
			if (collision.transform.tag != "Player" && !Scared)
				if (Broken != null) {
					GetComponent<SpriteRenderer>().sprite = Broken;
					GetComponent<AudioSource>().Play();
					Scared = true;
					StartCoroutine(Scare());
				}
		}

		private IEnumerator Scare() {
			yield return new WaitForSeconds(1);
			houseHolder.IncreaseFearLevel();
		}
	}
}
