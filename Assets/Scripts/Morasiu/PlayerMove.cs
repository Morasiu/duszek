﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public Sterring Sterring;
	public float HorizontalSpeed;
	public float VerticalSpeed;
	Vector3 target;

	void Start () {
		target = transform.position;
		// Just hack, ignore
		transform.position = new Vector3(transform.position.x + 0.1f, transform.position.y, transform.position.z);
	}
	
	void Update () {
		if(Sterring == Sterring.Mouse)
			MoveToMouse();
		else if (Sterring == Sterring.WSAD) {
			var horziontal = Input.GetAxis("Horizontal") * HorizontalSpeed;
			var vertical = Input.GetAxis("Vertical") * VerticalSpeed;

			transform.Translate(new Vector2(horziontal, vertical));
		}
	}

	private void MoveToMouse() {
		if (Input.GetMouseButtonDown(0)) {
			target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			target.z = transform.position.z;
		}

		transform.position = Vector3.MoveTowards(transform.position, target, VerticalSpeed * Time.deltaTime);
	}
}

public enum Sterring {
	Mouse,
	WSAD
}
