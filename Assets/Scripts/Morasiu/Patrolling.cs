﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrolling : MonoBehaviour {

	public bool ShouldPatrol = true;
	public float Speed = 3f;
	public float Sin = 20f;
	public float RotationSpeed = 0.2f;
	public Transform Left;
	public Transform Right;

	Vector2 _sinusPosition;
	private Vector3 _moveDirection = Vector3.right;
	// Update is called once per frame
	void Update () {
		if(ShouldPatrol) {
			_sinusPosition = transform.position;
			_sinusPosition.y += Mathf.Sin(Time.time * Sin) * Time.deltaTime;
			transform.position = _sinusPosition;
		}
	}

	private void LateUpdate() {
		if(ShouldPatrol) {
			if (transform.position.x > Right.position.x) {
				_moveDirection = -_moveDirection;

			} else if (transform.position.x < Left.position.x) {
				_moveDirection = -_moveDirection;
			}
			transform.Translate(_moveDirection * Speed * Time.deltaTime);
		}

	}
}
