﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour {

	public float TimeToDestroy = 3f;

	// Use this for initialization
	void Start () {
		//Destroy(gameObject, TimeToDestroy);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnCollisionEnter2D(Collision2D collision) {
		if(collision.transform.tag == "Player") {
			var playerController = collision.gameObject.GetComponent<PlayerController>();
			playerController.KillPlayer();
		}
	}

}
