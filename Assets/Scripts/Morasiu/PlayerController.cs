﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    List<GameObject> sprites;
    // 0- normal
    // 1- angel

    bool readyToChangeSprite;
    bool died = false;
    bool shouldBack = true;
    Vector3 pointA;
    Vector3 pointB;
    Vector3 target;
    public AudioSource angelsound;
    public Cinemachine.CinemachineVirtualCamera cam;
    Coroutine cor;
    public GameObject light;
    public bool gameOver;
    private void Start()
    {
        gameOver = false; 
    }
    void Update()
    {
        if (died)
        {
            pointA = new Vector3(transform.position.x, 6f, transform.position.z);
            pointB = new Vector3(transform.position.x, -2.5f, transform.position.z);

            GetComponent<Collider2D>().enabled = false;
            if (!readyToChangeSprite)
            {
                target = pointA;
            }
            else if (readyToChangeSprite)
            {
                target = pointB;
            }
            if (transform.position.y > 3.8f)
            {
                readyToChangeSprite = true;
                sprites[0].SetActive(false);
                sprites[1].SetActive(true);
            }
            if (transform.position.y < -2.3f && readyToChangeSprite)
            {
                if (cor == null)
                {
                    cor = StartCoroutine(GoToHeaven());
                    shouldBack = false;
                    angelsound.Play();
                    cam.Follow = null;
                    gameOver = true;
                }
            }
            if (transform.position.y > 3.8f && readyToChangeSprite && !shouldBack)
            {
                light.SetActive(false);
                this.gameObject.SetActive(false);
            }
            transform.position = Vector3.Slerp(transform.position, target, 0.06f);

        }
    }

    public void KillPlayer()
    {
        died = true;
        Debug.Log("DEAD :/");
    }
    IEnumerator GoToHeaven()
    {
        light.gameObject.SetActive(true);

        float x = 0f;
        while (transform.position.y < pointA.y - 0.4f)
        {
            transform.position = Vector3.Slerp(transform.position, pointA, x * 0.015f);
            var _newPosition = transform.position;
            _newPosition.x += (Mathf.Sin(x * 8) / 15f);
            x += Time.deltaTime;
            transform.position = _newPosition;
            yield return null;
        }
        yield return new WaitForSeconds(.1f);
    }
}
