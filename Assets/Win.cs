﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour {

	public HouseHolder mother;
	public HouseHolder Grandmother;

	public Transform target;

	public float Speed = 0.2f;
	public float Y = -6;
	public float TimeToLoadNewScene = 1.5f;

	private LockCameraZ LockCameraZ;
	private bool loadingMenu = false;
	private void Start() {
		LockCameraZ = GetComponent<LockCameraZ>();
	}

	// Update is called once per frame
	void Update () {
		if((int)mother.FearLevelValue >=3 && (int) Grandmother.FearLevelValue >= 2) {

			Debug.Log("WIN!");

			var cam = GetComponent<Cinemachine.CinemachineVirtualCamera>();
			var offset = cam.GetCinemachineComponent<Cinemachine.CinemachineTransposer>().m_FollowOffset;
			var a = new Vector3(-0.1f, 0, -1.5f);
			cam.GetCinemachineComponent<Cinemachine.CinemachineTransposer>().m_FollowOffset = Vector3.Lerp(offset, a, Speed);
			LockCameraZ.YPosition = Mathf.Lerp(LockCameraZ.YPosition, target.position.y, Speed);
			if (!loadingMenu) StartCoroutine(LoadMenu());
			//cam.GetCinemachineComponent<Cinemachine.CinemachineTransposer>().m_FollowOffset =
		}
	}

	IEnumerator LoadMenu() {
		loadingMenu = true;
		yield return new WaitForSeconds(TimeToLoadNewScene);
		SceneManager.LoadScene(0);
	}
}
