﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TV : InteractiveObject {

	public HouseHolder houseHolder;
	public Sprite Blur;

	public override void OnGhostInteraction() {
		GetComponent<SpriteRenderer>().sprite = Blur;
		GetComponent<AudioSource>().Play();
		StartCoroutine(Scare());
	}

	private IEnumerator Scare() {
		yield return new WaitForSeconds(1);
		houseHolder.IncreaseFearLevel();
	}
}
